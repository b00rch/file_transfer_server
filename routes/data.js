const fs      = require('fs');
const path    = require('path');
const express = require('express');

const config  = require('../config');

const router = express.Router();

/* GET users listing. */
router.get('/:path', function(req, res, next) {
	let file_path = decodeURIComponent(req.path.substr(1));
	file_path = path.resolve(config.path, file_path);
	console.log('downloading file '+ file_path.cyan);
	// protect local file inclusion
	if(file_path.search(config.path) === -1) {
		res.status(404).send('Not found'); // HTTP status 404: NotFound
	}

	if(!fs.existsSync(file_path)) {
		res.status(404).send('Not found'); // HTTP status 404: NotFound
	}
	// console.log('download start');
	res.download(file_path);

	// // test 1
	// res.send('ok');

	// // test 2
	// var file_path = path.join(config.path, 'YWJZsXNvZnQ=/name_card/123.jpg');
	// // var file_path = path.join(config.path, 'YWJZsXNvZnQ=/tmp/user77/123.jpg');
	// res.download(file_path);
});

module.exports = router;
