/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : socket.js
* Created at  : 2019-09-13
* Updated at  : 2019-09-13
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";
const debug  = require('debug')('able:socket');
const config = require('../../config');
const redis  = require('./redis');

module.exports = async socket =>  {
	const { server_name, server_address } = socket;

	socket.on('disconnect', async () => {
		console.log(`disconnect server\n\tname: ${server_name}\n\taddress: ${server_address}`);
		try {
			await redis.del('server-'+ server_name);
		}
		catch(err)  {
			console.error('On disconnect error:', err);
		}
	});
};