/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : auth.js
* Created at  : 2019-10-29
* Updated at  : 2019-10-29
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

const debug = require('debug')('able:auth');
const fs	= require('fs');
const Path  = require('path');

const config = require('../../config');
const redis  = require('./redis');

const { get_str_size } = require('../helpers/file_size_converter');

module.exports =async (socket, next) => {
	const client_host = socket.handshake.query.host;
	if(!client_host)  {
		next(new Error('client_host is not defined')); //cookie transmitted
		console.error('error', 'client_host is not defined');
		return false;
	}
	socket.client_host = client_host;
	socket.data_dir = socket.handshake.query.data_dir || '';

	const free_size = socket.handshake.query.free_size;
	socket.free_size_int = parseInt(free_size);
	socket.free_size_str = get_str_size(socket.free_size_int);

	const server_name = socket.handshake.query.name;
	const address = socket.handshake.address;

	try {
		let is_new;
		let keys = await redis.hkeys('server-'+ server_name);
		if (keys.length) {
			const saved_address = await redis.hget('server-'+server_name, 'address');
			if(address !== saved_address)  {
				next(new Error('Server name is duplicated')); //cookie transmitted
				console.error('error', 'Server name is duplicated');
				return false;
			}
			const res = await redis.hmset(
				'server-'+ server_name,
				{
					free_size_str : socket.free_size_str,
					free_size_int : socket.free_size_int,
					data_dir	  : socket.data_dir,
					client_host	  : client_host
				}
			);
			debug('server info updated.', res);
			is_new = 'reconnect';
			// keys = await redis.hkeys('server-'+ server_name);
			// debug(keys);
		}
		else {
			const res = await redis.hmset(
				'server-'+ server_name,
				{
					address       : address,
					name          : server_name,
					free_size_str : socket.free_size_str,
					free_size_int : socket.free_size_int,
					data_dir	  : socket.data_dir,
					client_host	  : client_host
				}
			);
			debug('server info inserted.', res);
			is_new = 'connect';
		}
		socket.server_name = server_name;
		socket.server_address = address;
			
		console.log(`${is_new} server
	name: ${server_name}
	free_size: ${socket.free_size_str}
	address: ${address}`);

		next();
	}
	catch(err) {
		next(err);  //No session id transmitted.
		return false;
	}
}