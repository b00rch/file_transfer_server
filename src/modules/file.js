/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : file.js
* Created at  : 2019-09-23
* Updated at  : 2019-09-23
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";
const Path   = require('path');
const Colors = require('colors');
const Axios  = require('axios');
const https  = require('https');
const debug  = require('debug')('able:file');

const { fs, cryptor, get_checksum, file_size_converter } = require('../helpers');
const { get_str_size } = file_size_converter;

const config = require('../../config');
const redis  = require('./redis');
const before3month = Date.now() - 90 * 86400 * 1000;

async function send_file_meta(key, data)  {
	let client_host;
	try {
		client_host = await redis.hget(key, 'client_host');
	}
	catch(err)  {
		console.log('storage client is disconnected.');
		return new Promise((resolve, reject) => {
			setTimeout(async () => {
				const res = await send_file_meta(key, data);
				resove(res);
			}, 3000);
		});
	}

	try {
		const res = await Axios({
			url: client_host +'/data',
			method: 'POST',
			responseType: 'json',
			httpsAgent: new https.Agent({ rejectUnauthorized: false }),
			data
			// responseType: 'stream'
		});
		debug(res.data);
		return res;
	}
	catch(err)  {
		console.log('storage client is disconnected. '+ err.message);
		return new Promise((resolve, reject) => {
			setTimeout(async () => {
				const res = await send_file_meta(key, data);
				resove(res);
			}, 3000);
		});
	}
}

async function recursive_send_files(key, data_dir) {
	const full_path = Path.join(config.path, data_dir);
	debug(full_path);
	// data_dir байгаа эсэхийг шалгана
	if (!fs.existsSync(full_path)) {
		return new Promise((resolve, reject) => {
			reject(new Error('client error status 500'))
		});
		// console.error('error', full_path.cyan +' directory is not exist.');
		// return false;
	}

	const list = fs.readdirSync(full_path);

	let ind, ext, file, abs_file, checksum, result, stat;
	for(ind in list)  {
		if(list[ind] === 'tmp')  continue;
		if(list[ind] === 'printPages')  continue;
		if(list[ind] === 'log')  continue;
		if(list[ind] === '.htaccess') continue;
		if(list[ind] === '.DS_Store') {
			fs.unlinkSync(Path.join(full_path, list[ind]));
			continue;
		}
		if(list[ind] === 'crlFile') continue;

		ext = Path.extname(list[ind]);
		if(ext === '.py') continue;
		// if(ext === '.php') continue;
		if(ext === '.log') continue;
		// if(ext === '.out') continue;

		file = Path.join(full_path, list[ind]);
		abs_file = (data_dir ? data_dir + Path.sep : '') + list[ind];

		if(file === Path.join(config.path,'result.txt')) continue;
		if(file === Path.join(config.path,'backuplist2.txt')) continue;

		if(fs.is_file(file)) {
			// statistics.all_files++;
			checksum = get_checksum(fs.readFileSync(file));

			const { ino, size, mtimeMs } =fs.lstatSync(file);
			// if(mtimeMs < before3month) continue;
			// statistics.checked_size += size;
			console.log('\n'+ checksum +' [size:'+ size +']\n'+ file.cyan);

			// send file meta data to client
			const res = await send_file_meta(key, {
				path: abs_file,
				size,
				checksum,
			});

			if(res.error)  {
				console.error('error from client', res.msg)
			}
		}
		else if(fs.is_dir(file))  {
			// console.log(abs_file +' is directory');
			await recursive_send_files(key, abs_file);
		}
		else if(fs.is_symbolic_link(file))  {
			console.log('\nsoft link: '.yellow + file.cyan);
		}
	}

	// return Promise.resolve();
	// return new Promise(resolve => setTimeout(() => {
	// 	resolve();
	// }, 10000));
}

async function start() {
	const keys = await redis.keys('server-*');
	if (!keys.length) {
		console.log('storage client is disconnected.');
		setTimeout(start, 3000);
		return;
	}
	

	keys.forEach(async key => {
		const data_dir = await redis.hget(key, 'data_dir');
		console.log('data_dir: ', data_dir);
		try {
			await recursive_send_files(key, data_dir);
		}
		catch(err)  {
			console.error('Error on recursive_send_files,', err);
		}
	});

	console.log('finished task');
	// setTimeout(start, 5000);
}

module.exports = start;