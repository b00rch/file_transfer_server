/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : file_size_converter.js
* Created at  : 2019-09-26
* Updated at  : 2019-09-26
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

function get_int_size(size) {
	if(typeof size !== 'string') return new Error("Not string size");

	const char = size.charAt(size.length -1).toLowerCase();
	size = parseInt(size);
	switch (char)  {
		case 'p':
			size *=1024;
		case 't':
			size *=1024;
		case 'g':
			size *=1024;
		case 'm':
			size *=1024;
		case 'k':
			size *=1024;
	}
	return size;
}

function get_str_size(size) {
	if(typeof size !== 'number') return new Error("Not number size");

	let max_size = 1024;
	let now_size = size;
	let char;

	for(char of ['b','K','M','G','T','P'])  {
		if(size < max_size) {
			return Math.round(now_size * 100) / 100 + char;
		}
		now_size /= 1024; 
		max_size *= 1024;
	}
	return new Error("Number size is too large");
}

module.exports = {
	get_int_size,
	get_str_size
}