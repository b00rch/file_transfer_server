/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : index.js
* Created at  : 2019-07-18
* Updated at  : 2019-07-18
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";
// const debug = require('debug')('able:main');
const config = require('../config');
const auth   = require('./modules/auth');

module.exports = async server => {
	// Listen Backup Server 
	const io = require('socket.io')(server);
	const socket_fn = require('./modules/socket');

	io.set('transports', config.socket);
	io.use(auth);
	io.on('connection', socket_fn);

	require('./modules/file')();
};